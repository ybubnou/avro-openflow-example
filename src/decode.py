import struct

def byte(value):
    return struct.pack("B", value)

def short(value):
    return struct.pack(">H", value)

def unsigned(value):
    return struct.pack(">L", value)

