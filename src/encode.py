import struct

def byte(value):
    return struct.unpack("B", value)[0]

def short(value):
    return struct.unpack(">H", value)[0]

def unsigned(value):
    return struct.unpack(">L", value)[0]

