from StringIO import StringIO
import avro.schema as schema
import avro.ipc as ipc
import avro.io as io


BIG_ENDIAN_SHORT_STRUCT = io.struct_class('!H')
BUFFER_SIZE = 8192
OPENFLOW_HEADER_LENGTH = 8

OPENFLOW_HEADER_SCHEMA = schema.parse("""
{
    "namespace": "openflow.header","name": "Header","type": "record",
    "fields": [
        {"name": "version", "type": 
            {"name": "Version","type": "fixed","size": 1}},
        {"name": "type", "type": 
            {"name": "Type", "type": "fixed", "size": 1}},
        {"name": "length", "type": 
            {"name": "Length", "type": "fixed", "size": 2}},
        {"name": "xid", "type": 
            {"name": "XId", "type": "fixed", "size": 4}}]
}
""")


class MessageReader(object):
    def __init__(self, reader):
        self._reader = reader

    reader = property(lambda self: self._reader)

    def read(self):
        buffer = StringIO()
        raw_header = self._reader.read(OPENFLOW_HEADER_LENGTH)

        if raw_header == '':
            raise ipc.ConnectionClosedException("Reader read 0 bytes.")

        decoder = io.BinaryDecoder(StringIO(raw_header))
        datum_reader = io.DatumReader(OPENFLOW_HEADER_SCHEMA)
        header = datum_reader.read(decoder)

        buffer.write(raw_header)
        message_length = BIG_ENDIAN_SHORT_STRUCT.unpack(header["length"])[0]

        while buffer.tell() < message_length:
            chunk = self.reader.read(message_length - buffer.tell())
            if chunk == '':
                raise ipc.ConnectionClosedException("Reader read 0 bytes.")
            buffer.write(chunk)

        return buffer.getvalue()


class MessageWriter(object):
    def __init__(self, writer):
        self._writer = writer

    writer = property(lambda self: self._writer)

    def write(self, message):
        message_length = len(message)
        total_bytes_sent = 0
        while message_length - total_bytes_sent > 0:
            if message_length - total_bytes_sent > BUFFER_SIZE:
                buffer_length = BUFFER_SIZE
            else:
                buffer_length = message_length - total_bytes_sent

            self.write_buffer(message[total_bytes_sent:(total_bytes_sent + buffer_length)])
            total_bytes_sent += buffer_length

    def write_buffer(self, chunk):
        self.writer.write(chunk)


class HeaderReader(object):
    def __init__(self, reader):
        self._reader = reader   

    def read(self):
        buffer = StringIO()
        raw_header = self._reader.read(OPENFLOW_HEADER_LENGTH)

        if raw_header == '':
            raise schema.AvroException("Reader read 0 bytes.")

        decoder = io.BinaryDecoder(StringIO(raw_header))
        datum_reader = io.DatumReader(OPENFLOW_HEADER_SCHEMA)
        return datum_reader.read(decoder)
    