from StringIO import StringIO
import avro.schema as schema
import avro.ipc as ipc
import avro.io as io


class LightWeightWriter(io.DatumWriter):
    def write_union(self, writers_schema, datum, encoder):
        print("DATUM %d" % ord(datum))

        message = ord(datum)
        index_of_schema = -1    

        #print(self.__class__.__name__, writers_schema.schemas[index_of_schema])

        for i, candidate_schema in enumerate(writers_schema.schemas):
            if io.validate(candidate_schema, datum) and \
                    candidate_schema.get_prop("default") == message:
               index_of_schema = i
        if index_of_schema < 0: raise io.AvroTypeException(writers_schema, datum)

        self.write_data(writers_schema.schemas[index_of_schema], datum, encoder)


class LightWeightReader(io.DatumReader):
    def read_union(self, writers_schema, readers_schema, decoder):
        #index_of_schema = int(decoder.read_long())
        # if index_of_schema >= len(writers_schema.schemas):
        #     fail_msg = "Can't access branch index %d for union with %d branches"\
        #         % (index_of_schema, len(writers_schema.schemas))
        #     raise SchemaResolutionException(fail_msg, writers_schema, readers_schema)

        selected_writers_schema = writers_schema.schemas[0]

        # read data
        return self.read_data(selected_writers_schema, readers_schema, decoder)

