import avro.protocol as protocol
import avro.schema as schema
import avro.ipc as ipc
import avro.io as io


class MessageTypes(object):
    def __init__(self, protocol):
        self._protocol = protocol
        self._messages = {}
        ofp_header_schema = protocol.types_dict["ofp_header"]
        ofp_types_list = ofp_header_schema.fields_dict["type"].type

        for schema in ofp_types_list.schemas:
            setattr(self, schema.name, schema.get_prop("default"))
            self._messages[schema.get_prop("default")] = schema.name[5:].lower()

    def __getitem__(self, item):
        return self._messages[item]

