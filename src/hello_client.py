from StringIO import StringIO
import avro.protocol as protocol
import avro.ipc as ipc
import avro.io as io
import openflow
import socket
import decode
import struct


PROTOCOL = protocol.parse(open("../avro/openflow.avpr").read())
ofp_types = openflow.types.MessageTypes(PROTOCOL)


class SocketTransceiver(object):
    def __init__(self, host, port):
        self.conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0)
        self.conn.connect((host, port))

    sock = property(lambda self: self.conn)
    remote_name = property(lambda self: self.sock.getsockname())

    def set_conn(self, new_conn):
        self._conn = new_conn
    
    conn = property(lambda self: self._conn, set_conn)

    def transceive(self, request):
        self.write_message(request)
        result = self.read_message()
        return result

    def read_message(self):
        response_reader = openflow.ipc.MessageReader(self.conn.makefile())
        response = response_reader.read()
        return response

    def write_message(self, message):
        self.conn.sendall(message)

    def close(self):
        self.conn.close()


class OpenflowRequestor(ipc.Requestor):
    def __init__(self, protocol, transceiver):
        ipc.Requestor.__init__(self, protocol, transceiver)    
        self._local_protocol = protocol
        self._remote_protocol = protocol

    def request(self, message_name, request_datum):
        buffer_writer = StringIO()
        buffer_encoder = io.BinaryEncoder(buffer_writer)
        self.write_call_request(message_name, request_datum, buffer_encoder)

        call_request = buffer_writer.getvalue()
        return self.issue_request(call_request, message_name, request_datum)

    def write_call_request(self, message_name, request_datum, encoder):
        message = self.local_protocol.messages.get(message_name)
        if message is None:
            raise schema.AvroException('Unknown message: %s' % message_name)

        print(message.request)
        self.write_request(message.request, request_datum, encoder)


    def read_call_response(self, message_name, decoder):
        remote_message_schema = self.remote_protocol.messages.get(message_name)
        if remote_message_schema is None:
            raise schema.AvroException('Unknown remote message: %s' % message_name)

        local_message_schema = self.local_protocol.messages.get(message_name)
        if local_message_schema is None:
            raise schema.AvroException('Unknown local message: %s' % message_name)

        writers_schema = remote_message_schema.response
        readers_schema = local_message_schema.response
        return self.read_response(writers_schema, readers_schema, decoder)

    def write_request(self, request_schema, request_datum, encoder):
        datum_writer = openflow.io.LightWeightWriter(request_schema)
        datum_writer.write(request_datum, encoder)

    def read_response(self, writers_schema, readers_schema, decoder):
        datum_reader = openflow.io.LightWeightReader(writers_schema, readers_schema)
        result = datum_reader.read(decoder)
        return result

    def issue_request(self, call_request, message_name, request_datum):
        call_response = self.transceiver.transceive(call_request)
        buffer_decoder = io.BinaryDecoder(StringIO(call_response))
        return self.read_call_response(message_name, buffer_decoder)




class RecordBuilder(object):
    def __init__(self, schema):
        self._schema = schema

    schema = property(lambda self: self._schema)

    def build(self, datum={}):
        buffer_writer = StringIO()
        buffer_encoder = io.BinaryEncoder(buffer_writer)
        datum_writer = io.DatumWriter(self.schema)
    
        record = {}

        for field_name, field in self.schema.fields_dict.iteritems():
            if field_name not in datum:
                if field.default is None:
                    raise io.AvroTypeException(self.schema, datum)
                else:
                    record[field_name] = str(field.default)
            else:
                record[field_name] = datum[field_name]

        datum_writer.write(record, buffer_encoder)
        return buffer_writer.getvalue()

        
ofp = protocol.parse(open("../avro/oflow.avpr").read())
header_schema = ofp.types_dict["ofp_hello_header"]

server_addr = ("localhost", 6633)
transceiver = SocketTransceiver(*server_addr)
call_request = header_schema.build_defaults({u"xid":u"\u00de\u00ad\u00be\u00ef"})
#call_request = header_schema.build_defaults({})


buffer_writer = StringIO()
buffer_encoder = io.BinaryEncoder(buffer_writer)
datum_writer = io.DatumWriter(header_schema)
datum_writer.write(call_request, buffer_encoder)
call_request = buffer_writer.getvalue()

call_request = struct.pack("%dB" % len(call_request), 
    *map(ord, list(call_request)))

call_response = transceiver.transceive(call_request)
print struct.unpack("8B", call_request)
print struct.unpack("8B", call_response)
        


#transceiver = SocketTransceiver(*server_addr)
#transceiver = None
#requestor = OpenflowRequestor(PROTOCOL, transceiver)

#message = {
    #"header": {
        #"version": decode.byte(openflow.version.OFP_VERSION),
        #"type": decode.byte(ofp_types.OFPT_HELLO),
        #"length": decode.short(8),
        #"xid": decode.unsigned(0xabcdefab)
    #}
#}

#print("Send", message)
#print("Received ", requestor.request("hello", {"message": message}))
#transceiver.close()

