from StringIO import StringIO
import avro.protocol as protocol
import avro.schema as schema
import avro.ipc as ipc
import avro.io as io
import SocketServer
import openflow
import decode
import encode


PROTOCOL = protocol.parse(open("../avro/openflow.avpr").read())
server_addr = ("localhost", 6633)


class OpenflowResponder(ipc.Responder):
    def __init__(self, protocol):
        ipc.Responder.__init__(self, protocol)
        self.ofp_types = openflow.types.MessageTypes(protocol)
        self._remote_protocol = protocol

    remote_protocol = property(lambda self: self._remote_protocol)

    def respond(self, call_request):
        buffer_header = StringIO(call_request)
        buffer_reader = StringIO(call_request)
        buffer_decoder = io.BinaryDecoder(buffer_reader)
        buffer_writer = StringIO()
        buffer_encoder = io.BinaryEncoder(buffer_writer)
        error = None

        try:
            if self._remote_protocol is None:  
                return buffer_writer.getvalue()

            header_reader = openflow.ipc.HeaderReader(buffer_header)
            header = header_reader.read()

            remote_message_name = self.ofp_types[encode.byte(header["type"])]
            remote_message = self._remote_protocol.messages.get(remote_message_name)

            if remote_message is None:
                fail_msg = 'Unknown remote message: %s' % remote_message_name
                raise schema.AvroException(fail_msg)

            local_message = self.local_protocol.messages.get(remote_message_name)

            if local_message is None:
                fail_msg = 'Unknown local message: %s' % remote_message_name
                raise schema.AvroException(fail_msg)

            writers_schema = remote_message.request
            readers_schema = local_message.request
            request = self.read_request(writers_schema, readers_schema, buffer_decoder)

            # perform server logic
            try:
                response = self.invoke(local_message, request)
            except ipc.AvroRemoteException, e:
                error = e
            except Exception, e:
                error = ipc.AvroRemoteException(str(e))

            if error is None:
                writers_schema = local_message.response
                self.write_response(writers_schema, response, buffer_encoder)
            else:
                writers_schema = local_message.errors
                self.write_error(writers_schema, error, buffer_encoder)
        except schema.AvroException, e:
            error = ipc.AvroRemoteException(str(e))
            buffer_encoder = io.BinaryEncoder(StringIO())
            buffer_encoder.write_boolean(True)
            self.write_error(SYSTEM_ERROR_SCHEMA, error, buffer_encoder)
        return buffer_writer.getvalue()


    def invoke(self, msg, req):
        if msg.name == "hello":
            message = req["message"]
            print("ACCEPTED %s" % message)
            
            resp = {
                "header": {
                    "version": decode.byte(4),
                    "type": decode.byte(self.ofp_types.OFPT_HELLO),
                    "length": decode.short(8),
                    "xid": message["header"]["xid"]
                }
            }

            return resp
        else:
            print("REJECTED %s" % message)
            raise schema.AvroException("unknown message: " + msg.getname())

    def read_request(self, writers_schema, readers_schema, decoder):
        datum_reader = openflow.io.LightWeightReader(writers_schema, readers_schema)
        return datum_reader.read(decoder)

    def write_response(self, writers_schema, response_datum, encoder):
        datum_writer = openflow.io.LightWeightWriter(writers_schema)
        datum_writer.write(response_datum, encoder)


class OpenflowHandler(SocketServer.StreamRequestHandler):
    def handle(self):
        self.responder = OpenflowResponder(PROTOCOL)
        call_request_reader = openflow.ipc.MessageReader(self.rfile)
        call_request = call_request_reader.read()

        resp_body = self.responder.respond(call_request)

        resp_writer = openflow.ipc.MessageWriter(self.wfile)
        resp_writer.write(resp_body)


server = SocketServer.TCPServer(server_addr, OpenflowHandler)
server.allow_reuse_addr = True

try:
    server.serve_forever()
finally:
    server.shutdown()

